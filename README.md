# Ransomware Response Kit
** Credits & Thankyou's **
> Gostaria de agradecer a Lawrence Abrams de BleepingComputer e Cody Johnston e Nathan Scott da EasySync por suas idéias, trabalho duro e comentários sobre este kit. Este kit é uma compilação de guias e vários recursos relacionados ao lidar com ransomware. Eu não sou o autor original de qualquer um desses recursos e não estou reivindicando crédito por isso. Grande parte do trabalho que está contido neste kit é dos membros de fóruns de computação e outros indivíduos. Estou apenas fornecendo um repositório central para essas informações. Vou fazer o meu melhor para manter contato com aqueles indivíduos que estão na vanguarda da análise de malware relacionados com ransomware e manter esta página atualizada. Obrigado!


Tenho compilado este kit para ser usado por profissionais de segurança e
Administradores de sistemas, para ajudar a simplificar o processo de
Respondendo a infecções ransomware.

Algumas das informações contidas neste kit são obsoletas devido à rápida evolução da natureza do ransomware. Vou fazer o meu melhor para mantê-lo atualizado com a ajuda da comunidade de malware em geral.



## Instruções

Você nunca deve pagar o resgate. Isso só reforçará esse tipo de ataque. De acordo com a maioria dos relatórios de inteligência de segurança, as empresas criminosas já estão fazendo grandes lucros com o ransomware.

>Em caso de infecção:

- Remova da rede o sistema infectado
- Tente identificar qual variante de ransomware você está infectado.
- Antes de remover a ameaça, crie uma cópia, se possível, para análise posterior, que pode ser necessária para a descriptografia de arquivos.
- Se possível, use pontos de restauração ou backups para retornar a um estado seguro depois de remover a ameaça.
- Se você identificou a variante de ransomware e uma ferramenta decrypter que está disponível para ele neste kit, você pode tentar utilizá-la.